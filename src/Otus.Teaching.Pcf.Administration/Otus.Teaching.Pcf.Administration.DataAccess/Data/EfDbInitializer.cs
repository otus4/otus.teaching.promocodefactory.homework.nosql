﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDBContext _mongoDBContext;

        public EfDbInitializer(IMongoDBContext mongoDBContext)
        {
            _mongoDBContext = mongoDBContext;
        }

        public void InitializeDb()
        {
            InitializeRoles();
            InitializeEmployees();
        }

        private void InitializeRoles()
        {
            var _dbCollection = _mongoDBContext.GetCollection<Role>(typeof(Role).Name);
            if (!_dbCollection.Find<Role>(s => true).Any())
            {
                _dbCollection.InsertMany(FakeDataFactory.Roles);
            }
        }

        private void InitializeEmployees()
        {
            var _dbCollection = _mongoDBContext.GetCollection<Employee>(typeof(Employee).Name);
            if (!_dbCollection.Find<Employee>(s => true).Any())
            {
                _dbCollection.InsertMany(FakeDataFactory.Employees);
            }
        }

    }
}