﻿namespace Otus.Teaching.Pcf.Administration.DataAccess.DatabaseSettings
{
    public interface IAdministrationDatabaseSettings
    {
        string EmployeesCollectionName { get; set; }
        string RolesCollectionName { get; set; }

        string ConnectionString { get; set; }
        string DatabaseName { get; set; }

    }
}
