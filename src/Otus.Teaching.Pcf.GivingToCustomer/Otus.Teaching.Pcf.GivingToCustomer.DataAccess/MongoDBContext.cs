﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.DatabaseSettings;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess
{
    public class MongoDBContext
    {
        private IMongoDatabase _db { get; set; }
        private MongoClient _mongoClient { get; set; }
        private readonly GivingDatabaseSettings databaseSettings;

        private string GetUrl
        {
            get
            {
                return $@"mongodb://{databaseSettings.Login}:{databaseSettings.Password}@{databaseSettings.ConnectionString}";
            }
        }


        public MongoDBContext(IConfiguration configurationData)
        {
            databaseSettings = configurationData.GetSection("AdministrationDatabaseSettings").Get<GivingDatabaseSettings>();
            _mongoClient = new MongoClient(GetUrl);
            _db = _mongoClient.GetDatabase(databaseSettings.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _db.GetCollection<T>(name);
        }

    }

    public interface IMongoDBContext
    {
        IMongoCollection<T> GetCollection<T>(string name);
    }

}
