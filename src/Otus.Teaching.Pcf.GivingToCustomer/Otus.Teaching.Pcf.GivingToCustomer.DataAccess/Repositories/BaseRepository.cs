﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoDBContext _mongoDBContext;
        private IMongoCollection<T> _dbCollection;
        public BaseRepository(IMongoDBContext context)
        {
            _mongoDBContext = context;
            _dbCollection = _mongoDBContext.GetCollection<T>(typeof(T).Name);
        }
        public async Task AddAsync(T entity)
        {
            await _dbCollection.InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _dbCollection.DeleteOneAsync(s => s.Id == entity.Id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbCollection.Find(s => true).ToListAsync();
        }

        public async Task<T> GetByIdAsync(string id)
        {
            return await _dbCollection.Find<T>(e => e.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _dbCollection.Find<T>(predicate).FirstOrDefaultAsync();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<string> ids)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task UpdateAsync(T entity)
        {
            await _dbCollection.ReplaceOneAsync(e => e.Id == entity.Id, entity);
        }

    }
}
