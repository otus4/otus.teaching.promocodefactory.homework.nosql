﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public static class FakeDataFactory
    {
        
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                //Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Id = "5fc370d65456c446f19e70b9",
                Name = "Театр",
            },
            new Preference()
            {
                //Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Id = "5fc3710a575d3ac55b74744a",
                Name = "Семья",
            },
            new Preference()
            {
                //Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Id = "5fc3713a4db9d9113a9eb9f4",
                Name = "Дети",
            }
        };

        public static List<Customer> Customers
        {
            get
            {
                //var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customerId = "5fc3715ca144895278060490";
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                CustomerId = customerId,
                                PreferenceId = "5fc3710a575d3ac55b74744a"
                            },
                            new CustomerPreference()
                            {
                                CustomerId = customerId,
                                PreferenceId = "5fc3713a4db9d9113a9eb9f4"
                            }
                        }
                    }
                };

                return customers;
            }
        }
    }
}