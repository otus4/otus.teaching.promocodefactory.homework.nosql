﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.DatabaseSettings
{
    public interface IGivingDatabaseSettings
    {
        string EmployeesCollectionName { get; set; }
        string RolesCollectionName { get; set; }

        string ConnectionString { get; set; }
        string DatabaseName { get; set; }

    }
}
