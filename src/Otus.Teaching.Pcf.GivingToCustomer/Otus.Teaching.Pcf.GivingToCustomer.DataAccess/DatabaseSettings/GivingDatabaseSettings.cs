﻿namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.DatabaseSettings
{
    public class GivingDatabaseSettings : IGivingDatabaseSettings
    {
        public string EmployeesCollectionName { get; set; }
        public string RolesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

    }
}
