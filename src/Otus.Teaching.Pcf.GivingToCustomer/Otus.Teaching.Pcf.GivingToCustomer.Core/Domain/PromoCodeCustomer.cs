﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer : BaseEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string PromoCodeId { get; set; }
        public virtual PromoCode PromoCode { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
