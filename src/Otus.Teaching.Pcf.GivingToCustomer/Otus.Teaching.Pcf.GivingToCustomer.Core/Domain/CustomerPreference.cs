﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class CustomerPreference
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string PreferenceId { get; set; }
        public virtual Preference Preference { get; set; }
    }
}