﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string PartnerId { get; set; }

        public virtual Preference Preference { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string PreferenceId { get; set; }

        public virtual ICollection<PromoCodeCustomer> Customers { get; set; }
    }
}